<?php

namespace Drupal\Tests\menu_link_weight\Functional;

use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\menu_link_weight\FunctionalJavascript\MenuLinkWeightTestTrait;

/**
 * Abstract class for menu link weight functional testing.
 *
 * @group menu_link_weight
 */
abstract class MenuLinkWeightFunctionalTestBase extends BrowserTestBase {

  use MenuLinkWeightTestTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'menu_link_weight',
    'node',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * The content menu link storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $contentMenuLinkStorage;

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The node type used in this test.
   *
   * @var string
   */
  protected $nodeType = 'page';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->menuLinkTree = $this->container->get('menu.link_tree');
    $this->menuLinkManager = $this->container->get('plugin.manager.menu.link');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->contentMenuLinkStorage = $entity_type_manager->getStorage('menu_link_content');
    $this->moduleInstaller = $this->container->get('module_installer');
    $this->state = $this->container->get('state');

    $this->drupalPlaceBlock('system_menu_block:tools');

    NodeType::create([
      'type' => $this->nodeType,
      'name' => $this->nodeType,
      'third_party_settings' => [
        'menu_ui' => [
          'available_menus' => ['tools'],
          'parent' => 'tools:',
        ]
      ]
    ])->save();

    $permissions = [
      'administer menu',
      "create {$this->nodeType} content",
      "edit own {$this->nodeType} content",
    ];

    // Create user.
    $user = $this->drupalCreateUser($permissions);
    // Log in user.
    $this->drupalLogin($user);
  }

}
