<?php

namespace Drupal\Tests\menu_link_weight\FunctionalJavascript;

/**
 * Tests the functionality of the Menu Link Weight module.
 *
 * @group menu_link_weight
 */
class MenuLinkWeightNodeJavascriptTest extends MenuLinkWeightJavascriptTestBase {

   /**
   * Tests menu functionality.
   */
  public function testMenuFunctionality() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet("/node/add/{$this->nodeType}");
    $page->checkField('Provide a menu link');

    $assert_session->pageTextContains('Change the weight of the links within the Tools menu');

    $select_xpath = $this->cssSelectToXpath('[data-drupal-selector="edit-menu-menu-parent"]');
    $this->getSession()->getDriver()->selectOption($select_xpath, 'tools:node.add_page');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->pageTextContains('Change the weight of the links within the Add content menu');
  }

}
